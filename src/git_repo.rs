use std::error::Error;
use std::path::Path;

use git2::Repository;

use crate::git_commit::*;
use crate::git_tag::*;

pub struct GitRepo {
    repo: Repository,
}

impl GitRepo {
    pub fn new<P>(path: P) -> Result<Self, Box<dyn Error>>
        where P: AsRef<Path>
    {
        let repo = Repository::open(path)?;
        Ok(Self {
            repo,
        })
    }

    pub fn tags(&self) -> Result<Vec<GitTag>, Box<dyn Error>> {
        let tag_names = self.repo.tag_names(None)?;
        let tags = tag_names.iter()
            .map(|tag_name_opt| {
                tag_name_opt
                    .ok_or("Tag name is not valid UTF-8.")
                    .map(|tag_name| GitTag::new(tag_name.to_string()))
            })
            .collect::<Result<_, _>>()?;
        Ok(tags)
    }

    pub fn versions(&self) -> Result<Vec<GitTagVersion>, Box<dyn Error>> {
        let versions = self.tags()?.into_iter()
            .filter_map(|tag| tag.version())
            .collect();
        Ok(versions)
    }

    pub fn latest_version(&self) -> Result<Option<GitTagVersion>, Box<dyn Error>> {
        let mut versions = self.versions()?;
        versions.sort();
        let version = versions.pop();
        Ok(version)
    }

    pub fn parse_rev_single(&self, rev: &str) -> Result<git2::Oid, Box<dyn Error>> {
        let oid = self.repo.revparse_single(rev)?
            .peel_to_commit()?
            .id();
        Ok(oid)
    }

    pub fn log(&self, from_oid: Option<git2::Oid>) -> Result<Vec<GitCommit>, Box<dyn Error>> {
        let mut revwalk = self.repo.revwalk()?;
        revwalk.set_sorting(git2::Sort::TIME);
        revwalk.simplify_first_parent();

        match from_oid {
            Some(from_oid) => {
                revwalk.push_head()?;
                revwalk.push(from_oid)?;
                revwalk.hide(from_oid)?;
            }
            None => {
                revwalk.push_head()?;
            }
        }

        let commits = revwalk
            .map(|oid| {
                let oid = oid?;
                let commit = self.repo.find_commit(oid)?;
                let message = commit.message().ok_or("Commit message is not valid UTF-8.")?;
                let is_merge = commit.parent_count() > 1;
                Ok(GitCommit::new(message.to_string(), is_merge))
            })
            .collect::<Result<_, Box<dyn Error>>>()?;

        Ok(commits)
    }
}
