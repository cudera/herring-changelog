use regex::Regex;

use crate::git_commit::*;

pub struct CommitParser {
    gitlab_pattern: Regex,
}

impl CommitParser {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn parse(&self, commit: GitCommit) -> ParsedCommit {
        if let Some(caps) = self.gitlab_pattern.captures(&commit.message) {
            ParsedCommit {
                message: caps["message"].to_string(),
                merge_request: Some(caps["merge_request"].to_string()),
            }
        } else {
            ParsedCommit {
                message: commit.message,
                merge_request: None,
            }
        }
    }
}

impl Default for CommitParser {
    fn default() -> Self {
        let gitlab_pattern = Regex::new(r"^Merge branch '(.+?)' into '(.+?)'\n{2}(?P<message>[\S\s]*)\n{2}See merge request (?P<merge_request>.+)$").unwrap();
        Self {
            gitlab_pattern,
        }
    }
}

pub struct ParsedCommit {
    pub message: String,
    pub merge_request: Option<String>,
}

impl ParsedCommit {
    pub fn message_first_line(&self) -> String {
        self.message.as_str().lines()
            .next().unwrap().to_string()
    }

    pub fn message_single_line(&self) -> String {
        self.message.as_str().lines()
            .filter(|&l| !l.is_empty())
            .collect::<Vec<&str>>()
            .join(" / ")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gitlab_merge() {
        let parser = CommitParser::new();
        let message = "Merge branch 'change1' into 'master'\n\nMR title 1\n\nSee merge request cudera/test/changelog-test!3";
        let commit = GitCommit::new(message.to_string(), true);
        let parsed = parser.parse(commit);

        assert_eq!(parsed.message, "MR title 1".to_string());
        assert_eq!(parsed.message_first_line(), "MR title 1".to_string());
        assert_eq!(parsed.message_single_line(), "MR title 1".to_string());
        assert_eq!(parsed.merge_request, Some("cudera/test/changelog-test!3".to_string()));
    }

    #[test]
    fn one_line() {
        let parser = CommitParser::new();
        let message = "Single line";
        let commit = GitCommit::new(message.to_string(), true);
        let parsed = parser.parse(commit);

        assert_eq!(parsed.message, "Single line".to_string());
        assert_eq!(parsed.message_first_line(), "Single line".to_string());
        assert_eq!(parsed.message_single_line(), "Single line".to_string());
        assert_eq!(parsed.merge_request, None);
    }

    #[test]
    fn multi_line() {
        let parser = CommitParser::new();
        let message = "First line\n\nParagraph line 1\nParagraph line 2";
        let commit = GitCommit::new(message.to_string(), true);
        let parsed = parser.parse(commit);

        assert_eq!(parsed.message, "First line\n\nParagraph line 1\nParagraph line 2".to_string());
        assert_eq!(parsed.message_first_line(), "First line".to_string());
        assert_eq!(parsed.message_single_line(), "First line / Paragraph line 1 / Paragraph line 2".to_string());
        assert_eq!(parsed.merge_request, None);
    }
}
