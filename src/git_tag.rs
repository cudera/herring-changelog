use std::cmp::Ordering;

use semver::Version;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GitTag {
    pub name: String,
}

impl GitTag {
    pub fn new(name: String) -> Self {
        Self {
            name,
        }
    }

    pub fn version(&self) -> Option<GitTagVersion> {
        let version_str = self.name.replace(r#"^v"#, "");
        Version::parse(&version_str).ok()
            .map(|version| {
                GitTagVersion {
                    name: self.name.clone(),
                    version,
                }
            })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Ord)]
pub struct GitTagVersion {
    pub name: String,
    pub version: Version,
}

impl PartialOrd for GitTagVersion {
    fn partial_cmp(&self, other: &GitTagVersion) -> Option<Ordering> {
        Some(self.version.cmp(&other.version))
    }
}
