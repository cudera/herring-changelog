#[derive(Debug, Clone)]
pub struct GitCommit {
    pub message: String,
    pub is_merge: bool,
}

impl GitCommit {
    pub fn new(
        message: String,
        is_merge: bool,
    ) -> Self {
        Self {
            message,
            is_merge,
        }
    }
}
