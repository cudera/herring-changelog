#![deny(warnings)]
use std::error::Error;
use std::path::PathBuf;

use structopt::StructOpt;

use crate::commit_parser::*;
use crate::git_repo::*;

pub mod git_tag;
pub mod git_repo;
pub mod git_commit;
pub mod commit_parser;

#[derive(StructOpt, Debug)]
pub struct Opt {
    /// Git repo folder
    #[structopt(long = "workdir", env = "WORKSPACE_ROOT")]
    pub workdir: PathBuf,

    /// Start changelog from given git revision
    #[structopt(long = "from-rev", conflicts_with = "from-last-version")]
    pub from_rev: Option<String>,

    /// Start changelog from the last tag that is a SemVer string
    #[structopt(long = "from-last-version", conflicts_with = "from-rev")]
    pub from_last_version: bool,
}

fn main() {
    let opt: Opt = Opt::from_args();
    let result = changelog(opt);
    if let Err(error) = result {
        eprintln!("ERROR: {}", error);
        std::process::exit(1);
    }
}

fn changelog(opt: Opt) -> Result<(), Box<dyn Error>> {
    let repo = GitRepo::new(opt.workdir)?;
    let from_rev = match (opt.from_rev, opt.from_last_version) {
        (Some(from_rev), _) => Some(from_rev),
        (_, true) => repo.latest_version()?.map(|tag| tag.name),
        _ => None,
    };
    let from_oid = match from_rev {
        Some(rev) => Some(repo.parse_rev_single(&rev)?),
        None => None,
    };

    let log = repo.log(from_oid)?;
    let commit_parser = CommitParser::new();
    let changelog: String = log.into_iter()
        .filter(|commit| commit.is_merge)
        .map(|commit| {
            let parsed = commit_parser.parse(commit);
            let desc = match &parsed.merge_request {
                Some(merge_request) => format!("{} ({})", parsed.message_single_line(), merge_request),
                None => parsed.message_single_line(),
            };
            format!("- {}", desc)
        })
        .collect::<Vec<String>>()
        .join("\n");
    println!("{}", changelog);
    Ok(())
}
