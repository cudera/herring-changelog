#![deny(warnings)]

#[macro_use]
extern crate insta;

use std::process::Command;
use std::str;

use assert_cmd::prelude::*;

#[test]
fn all() {
    let assert = Command::cargo_bin("herring-changelog")
        .unwrap()
        .args(&["--workdir=./tests/fixtures/example-repo"])
        .assert()
        .success();

    assert_snapshot!(str::from_utf8(&assert.get_output().stdout).unwrap(), @r###"
   ⋮- Custom multi-line MR commit message / paragraph 1 line 2 / paragraph 2 line 1 / paragraph 2 line 2
   ⋮- MR title 6 (cudera/test/changelog-test!8)
   ⋮- Custom single-line MR commit message
   ⋮- MR title 4 (cudera/test/changelog-test!6)
   ⋮- MR title 3 (cudera/test/changelog-test!5)
   ⋮- MR title 2 (cudera/test/changelog-test!4)
   ⋮- MR title 1 (cudera/test/changelog-test!3)
    "###);
}

#[test]
fn from_rev_tag() {
    let assert = Command::cargo_bin("herring-changelog")
        .unwrap()
        .args(&["--workdir=./tests/fixtures/example-repo", "--from-rev=0.2.0"])
        .assert()
        .success();

    assert_snapshot!(str::from_utf8(&assert.get_output().stdout).unwrap(), @r###"
   ⋮- Custom multi-line MR commit message / paragraph 1 line 2 / paragraph 2 line 1 / paragraph 2 line 2
   ⋮- MR title 6 (cudera/test/changelog-test!8)
   ⋮- Custom single-line MR commit message
   ⋮- MR title 4 (cudera/test/changelog-test!6)
    "###);
}

#[test]
fn from_rev_commit() {
    let assert = Command::cargo_bin("herring-changelog")
        .unwrap()
        // Commit 'ad16c4b' is the tag '0.2.0'
        .args(&["--workdir=./tests/fixtures/example-repo", "--from-rev=ad16c4b"])
        .assert()
        .success();

    assert_snapshot!(str::from_utf8(&assert.get_output().stdout).unwrap(), @r###"
   ⋮- Custom multi-line MR commit message / paragraph 1 line 2 / paragraph 2 line 1 / paragraph 2 line 2
   ⋮- MR title 6 (cudera/test/changelog-test!8)
   ⋮- Custom single-line MR commit message
   ⋮- MR title 4 (cudera/test/changelog-test!6)
    "###);
}

#[test]
fn from_rev_invalid() {
    let assert = Command::cargo_bin("herring-changelog")
        .unwrap()
        .args(&["--workdir=./tests/fixtures/example-repo", "--from-rev=INVALID_REVISION"])
        .assert()
        .failure();

    assert_snapshot!(str::from_utf8(&assert.get_output().stderr).unwrap(), @r###"
    ERROR: revspec 'INVALID_REVISION' not found; class=Reference (4); code=NotFound (-3)
    "###);
}

#[test]
fn from_last_version() {
    let assert = Command::cargo_bin("herring-changelog")
        .unwrap()
        .args(&["--workdir=./tests/fixtures/example-repo", "--from-last-version"])
        .assert()
        .success();

    assert_snapshot!(str::from_utf8(&assert.get_output().stdout).unwrap(), @r###"
   ⋮- Custom multi-line MR commit message / paragraph 1 line 2 / paragraph 2 line 1 / paragraph 2 line 2
   ⋮- MR title 6 (cudera/test/changelog-test!8)
    "###);
}
