herring-changelog (WIP)
=======================

Printing a changelog from git commit history.

```console
$ cargo run -q -- --workdir tests/fixtures/example-repo --next-version 2.0.0
- Custom multi-line MR commit message / paragraph 1 line 2 / paragraph 2 line 1 / paragraph 2 line 2
- MR title 6 (cudera/test/changelog-test!8)
- Custom single-line MR commit message
- MR title 4 (cudera/test/changelog-test!6)
- MR title 3 (cudera/test/changelog-test!5)
- MR title 2 (cudera/test/changelog-test!4)
- MR title 1 (cudera/test/changelog-test!3)
```

Installation
------------

```
cargo install --git https://gitlab.com/cudera/herring-changelog.git
```

Development
-----------

Run tests:
```
cargo test
```
